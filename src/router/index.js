import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/TravelInsurance',
    name: 'TravelInsurance',
    component: function () {
      return import('../views/TravelInsurance.vue')
    }
  },
  {
    path: '/ProductList',
    name: 'ProductList',
    props: true,
    component: function () {
      return import('../views/ProductList.vue')
    }
  }
  ,
  {
    path: '/CustomerList',
    name: 'CustomerList',
    component: function () {
      return import('../views/CustomerList.vue')
    }
  }
  ,
  {
    path: '/WaitForPayment',
    name: 'WaitForPayment',
    component: function () {
      return import('../views/WaitForPayment.vue')
    }
  }
  ,
  {
    path: '/WaitingList',
    name: 'WaitingList',
    props: true,
    component: function () {
      return import('../views/WaitingList.vue')
    }
  }
  ,
  {
    path: '/MyInsurance',
    name: 'MyInsurance',
    component: function () {
      return import('../views/MyInsurance.vue')
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
